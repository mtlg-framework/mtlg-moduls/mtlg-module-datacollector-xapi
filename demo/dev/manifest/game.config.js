MTLG.loadOptions({
    "width":1920, //game width in pixels
    "height":1080, //game height in pixels
    "languages":["en","de"], //Supported languages. First language should be the most complete.
    "countdown":180, //idle time countdown
    "fps":"60", //Frames per second
    "playernumber":4, //Maximum number of supported players
    "webgl": false, //Using webgl enables using more graphical effects
    "xapi":{//Configurations for the xapidatacollector. Endpoint and auth properties are required for the xapidatacollector module to work.
        "endpoint": "http://lrs.elearn.rwth-aachen.de/data/xAPI/", //The endpoint of the LRS where XAPI statements are send
        "auth": "Basic NjM1ZWQyMGY2YWY0OGUyYjc1NmIxOWZkNzFmYjE2ZTZmY2ZiOTk3NTozMzU4Njk0OWJjYTM0Yjc4MjI1N2M1Y2Q5YzhiYjNmMjhkYTQ3ZTQy", //Authentification token for LRS
        "gamename": "DemoDatacollectorGame", //The name of the game can be transmitted as the platform attribute
        "actor": { //A default actor can be specified to be used when no actor is present for a statement
            objectType : "Agent",
            id : "mailto:defaultActor@test.com",
            name : "DefaultActor"
        }
    },
});
