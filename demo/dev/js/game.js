/*
 * Demo game to debug the functions of the xapidatacollector module and showcase the usage of the xapidatacollector module.
 */

// expose framework for debugging
window.MTLG = MTLG;
window.window.createjs = createjs;
// ignite the framework
document.addEventListener("DOMContentLoaded", MTLG.init);

// add modules
require('mtlg-moduls-utilities');
require('mtlg-modul-xapidatacollector');

// load configuration
require('../manifest/device.config.js');
require('../manifest/game.config.js');
require('../manifest/game.settings.js');

MTLG.addGameInit(() => {

    //register the only level
    MTLG.lc.registerLevel(() => {
        let background = new createjs.Shape();
        background.graphics.beginFill("white").drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);

        //Create a button to send a XAPI statement with an activity object to the LRS
        let sendActivityObjectButton = MTLG.utils.uiElements.addButton(
            {text: "Send Activity Object", sizeX: 300, sizeY: 70}, () => {
                //This is how the XAPIDatacollector-Module can be used to send a statement with an activity as object
                let stmt = {
                    actor: {
                        objectType: "Agent",
                        id: "mailto:test@test.com",
                        name: "Testuser"
                    },
                    verb: {
                        id : "http://tested",
                        display : {en : "tested"}
                    },
                    object: {
                        id : "http://sendActivityStatementFunction"
                    }
                };
                MTLG.xapidatacollector.sendStatement(stmt).then(function (stmtID) {
                    console.log(stmtID);
                }).catch(function (error){
                    console.log(error.message);
                });
            }).set({x:.5*MTLG.getOptions().width, y:.5*MTLG.getOptions().height});

        /*Create a button to send a XAPI statement with an agent as object to the LRS. Also show how a result and context
        can be added to the statement as well as extensions for them.*/
        let sendAgentObjectButton = MTLG.utils.uiElements.addButton(
            {text: "Send Agent Object", sizeX: 300, sizeY: 70}, () => {
                //This is how the XAPIDatacollector-Module can be used to send a statement with an agent object
                let stmt = {
                    actor: {
                        objectType: "Agent",
                        id: "mailto:test@test.com",
                        name: "Testuser"
                    },
                    verb: {
                        id: "http://helped",
                        display: {en: "helped"}
                    },
                    object: {
                        objectType: "Agent",
                        id: "mailto:testuser2@test.com",
                        name: "Testuser2"
                    },
                    result: {
                        success: true,
                        extensions: {
                            "http://scoreResultExtension": "80"
                        }
                    },
                    context: {
                        statement: {
                            objectType: "StatementRef",
                            id: "82e58ba2-b4fb-4fb7-b2bd-b891e0bd3690"
                        },
                        extensions: {
                            "http://levelContextExtension": "level 1"
                        }
                    }
                };
                MTLG.xapidatacollector.sendStatement(stmt).then(function (stmtID) {
                    console.log(stmtID);
                }).catch(function (error) {
                    console.log(error.message);
                });
            }).set({x:.7*MTLG.getOptions().width, y:.5*MTLG.getOptions().height});

        //Create a button to send a statement with the default actor specified in manifest/game.config.js
        let sendDefaultActorButton = MTLG.utils.uiElements.addButton(
            {text: "Send with Default Actor", sizeX: 300, sizeY: 70}, () => {
                /*This is how the XAPIDatacollector-Module can be used to send a statement with the default actor by omitting
                the actor completely in the statement.*/
                let stmt = {
                    verb: {
                        id : "http://tested",
                        display : {en : "tested"}
                    },
                    object: {
                        id : "http://sendActivityStatementFunction"
                    }
                };
                let newDefaultActor = {
                    objectType : "Agent",
                        id : "mailto:newDefaultActor@test.com",
                        name : "NewDefaultActor"
                };
                if (MTLG.xapidatacollector.changeDefaultActor(newDefaultActor)) {
                    MTLG.xapidatacollector.sendStatement(stmt).catch(function (error) {
                        console.log(error.message);
                    });
                }
            }).set({x:.3*MTLG.getOptions().width, y:.5*MTLG.getOptions().height});

        //Create a button to get a statement via statement id and output it through the console
        let getButton = MTLG.utils.uiElements.addButton({text: "Get Statement", sizeX: 300, sizeY: 70}, () =>{
            MTLG.xapidatacollector.getStatementById('de83d4d9-3108-47f4-be8e-21efab8445ab').then(function (stmt) {
                console.log(JSON.stringify(stmt));
            }).catch(function (error) {
                console.log(error.message);
            })
        }).set({x:.5*MTLG.getOptions().width, y:.7*MTLG.getOptions().height});

        //Create a button to retrieve statements via searchParameters
        let searchButton = MTLG.utils.uiElements.addButton({text: "Search For Attributes", sizeX: 300, sizeY: 70}, () =>{
            MTLG.xapidatacollector.getStatements({registration: "d75aa1dd-6a4e-42a8-b6e2-e1614dacef41"}).then(function (stmts) {
                for (let stmt in stmts) {
                    console.log(JSON.stringify(stmts[stmt]));
                }
            }).catch(function (error) {
                console.log(error.message);
            })
        }).set({x:.5*MTLG.getOptions().width, y:.3*MTLG.getOptions().height});

        //Add all buttons to the game's level
        MTLG.getStageContainer().addChild(background, sendActivityObjectButton, sendAgentObjectButton, sendDefaultActorButton,
            getButton, searchButton);
    }, () => true);
});
